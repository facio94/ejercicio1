<?php
require 'Connect.php';
$Sql = 'SELECT a.id as id, b.nombre as marca, a.nombre as nombre, a.descripcion as descripcion
FROM auto a INNER JOIN marcas b ON a.idMarcas = b.id';
$Consulta = $Conn->prepare($Sql);
$Consulta->execute();
$personas = $Consulta->fetchAll(PDO::FETCH_OBJ);
?>



<?php require 'header.php';  ?>

<div class="container">
  <div class="card mt-5">
    <div class="card-header">
      <h2>Busqueda por auto</h2>
    </div>
    <div class="card-body">
      <table id="example" name="example" class="display data-results table table-striped table-hover table-bordered">
        <thead>
          <tr>
            <th>Marca</th>
            <th>Nombre</th>
            <th>Descripcion</th>
          </tr>
        </thead>

<tbody>
  <?php foreach ($personas as $pers):?>
  <tr>

    <td><?= $pers->marca; ?></td>
    <td><?= $pers->nombre; ?></td>
    <td><?= $pers->descripcion; ?></td>
  </tr>
<?php endforeach; ?>
</tbody>
      </table>
    </div>
  </div>
</div>



<?php require 'footer.php';  ?>

<script type="text/javascript">
$(document).ready(function() {
    $('#example').DataTable({
      "language": {
          "lengthMenu": "Mostrar _MENU_ registros por pagina",
          "zeroRecords": "No se encontraron resultados en su busqueda",
          "searchPlaceholder": "Buscar registros",
          "info": "Mostrando registros de _START_ al _END_ de un total de  _TOTAL_ registros",
          "infoEmpty": "No existen registros",
          "infoFiltered": "(filtrado de un total de _MAX_ registros)",
          "search": "Buscar:",
          "paginate": {
            "first":    "Primero",
            "last":    "Último",
            "next":    "Siguiente",
            "previous": "Anterior"
        },
      }
    });
} );
</script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">
