
<?php require 'Connect.php';

$Sql = 'SELECT a.id as id, b.nombre as marca, a.nombre as nombre, a.descripcion as descripcion,
a.tipoCombustible as combu, a.cantidadPuertas as puertas, a.precio as precio
FROM auto a INNER JOIN marcas b ON a.idMarcas = b.id';
$Consulta = $Conn->prepare($Sql);
$Consulta->execute();
$personas = $Consulta->fetchAll(PDO::FETCH_OBJ);
?>

<?php require 'header.php';  ?>
<div class="container">
  <div class="card mt-5">
    <div class="card-header">
      <h2>Autos registrados en el sistema</h2>
    </div>
    <div class="card-body">
      <table class="table table-bordered">
        <tr>
          <th>Id</th>
          <th>Marca</th>
          <th>Nombre</th>
          <th>Descripcion</th>
          <th>Combustible</th>
          <th>N. Puertas</th>
          <th>Precio</th>
          <th>Accion</th>


        </tr>
        <?php foreach ($personas as $pers):?>
        <tr>
          <td><?= $pers->id; ?></td>
          <td><?= $pers->marca; ?></td>
          <td><?= $pers->nombre; ?></td>
          <td><?= $pers->descripcion; ?></td>
          <td><?= $pers->combu; ?></td>
          <td><?= $pers->puertas; ?></td>
          <td><?= $pers->precio; ?></td>

          <td>
            <a href="edit.php?id=<?= $pers->id?>" class="btn btn-info">Editar</a>
            <a href="delete.php?id=<?= $pers->id?>" class="btn btn-danger">Eliminar</a>
          </td>
        </tr>
      <?php endforeach; ?>
      </table>
    </div>
  </div>
</div>

<?php require 'footer.php';  ?>
