<?php

require 'Connect.php';
$id = $_GET['id'];
$Sql = 'SELECT * FROM auto WHERE id=:id ';
$Consulta = $Conn->prepare($Sql);
$Consulta->execute([':id' => $id]);
$p = $Consulta->fetch(PDO::FETCH_OBJ);

$Sql7 = 'SELECT * FROM marcas';
$Consulta = $Conn->prepare($Sql7);
$Consulta->execute();
$p33 = $Consulta->fetchAll(PDO::FETCH_OBJ);

$Sql1 = 'SELECT b.id as id, b.nombre as marca
FROM auto a INNER JOIN marcas b ON a.idMarcas = b.id WHERE a.id=:id';
$Consulta = $Conn->prepare($Sql1);
$Consulta->execute([':id' => $id]);
$p11 = $Consulta->fetch(PDO::FETCH_OBJ);


if (isset($_POST["button"])) {
  $marca = $_POST["marca"];
  $nombre = $_POST["nombre"];
  $descripcion = $_POST["descripcion"];
  $combu = $_POST["combu"];
  $puertas = $_POST["puertas"];
  $precio= $_POST["precio"];

  $Sql = 'UPDATE auto SET idMarcas=:m,nombre=:n,descripcion=:d,tipoCombustible=:t,cantidadPuertas=:c,precio=:p WHERE id=:id';
  $Consulta = $Conn->prepare($Sql);
  if ($Consulta->execute([':m'=>$marca,':n'=>$nombre,':d'=>$descripcion,':t'=>$combu,
  ':c'=>$puertas,':p'=>$precio,':id'=>$id])) {

    header('Location: index.php');
  }
}

 ?>

<?php require 'header.php' ?>
<div class="container">
  <div class="card mt-5">
    <div class="card-header">
      <h2>Actualizar datos</h2>
    </div>
    <div class="card-body">

      <form class="" action="" method="post">

        <div class="form-group">
          <label for="email">Marca</label>
          <select class="form-control" name="marca" id="marca">
            <option hidden value="<?= $p11->id ; ?>" ><?= $p11->marca; ?></option>
            <?php foreach ($p33 as $pe):?>
                      <option value="<?= $pe->id; ?>"><?= $pe->nombre; ?> </option>
                      <?php endforeach; ?>
            </select>
        </div>
        <div class="form-group">
          <label for="nombre">Nombre</label><input value="<?= $p->nombre; ?>" placeholder="<?= $p->nombre; ?>" type="text" name="nombre" id="nombre" class="form-control">
        </div>
        <div class="form-group">
          <label for="email">descripcion</label><input value="<?= $p->descripcion; ?>" placeholder="<?= $p->descripcion; ?>" type="text" name="descripcion" class="form-control">
        </div>
        <div class="form-group">
          <label for="email">Combustible</label><input value="<?= $p->tipoCombustible; ?>" placeholder="<?= $p->tipoCombustible; ?>" type="text" name="combu" id="combu" class="form-control">
        </div>
        <div class="form-group">
          <label for="email">Puertas</label><input value="<?= $p->cantidadPuertas; ?>" placeholder="<?= $p->cantidadPuertas; ?>" type="number" name="puertas" id="puertas" class="form-control">
        </div>
        <div class="form-group">
          <label for="email">Precio</label><input value="<?= $p->precio; ?>" placeholder="<?= $p->precio; ?>" type="number" name="precio" id="precio" class="form-control">
        </div>

        <div class="form-group">
          <button type="submit" name="button" class="btn btn-info">Actualizar datos</button>
        </div>
      </form>
    </div>
  </div>
</div>

<?php require 'footer.php' ?>
