<?php
require 'Connect.php';
$Mensaje = '';
$Sql1 = 'SELECT  nombre AS marca, id AS id FROM marcas';
$Consulta = $Conn->prepare($Sql1);
$Consulta->execute();
$s = $Consulta->fetchAll(PDO::FETCH_OBJ);


if (isset($_POST["button"])) {
  $marca = $_POST["marca"];
  $nombre = $_POST["nombre"];
  $descripcion = $_POST["descripcion"];
  $combu = $_POST["combu"];
  $puertas = $_POST["puertas"];
  $precio= $_POST["precio"];


  $Sql = 'INSERT INTO auto(idMarcas,nombre,descripcion,tipoCombustible,cantidadPuertas,precio)
  VALUES (:m,:n,:d,:t,:c,:p)';
  $Consulta = $Conn->prepare($Sql);
  if ($Consulta->execute([':m'=>$marca,':n'=>$nombre,':d'=>$descripcion,':t'=>$combu,
  ':c'=>$puertas,':p'=>$precio])) {
    // code...
    $Mensaje = 'Datos insertados correctamente';
    header('Location: index.php');
  }
}

 ?>

<?php require 'header.php' ?>
<div class="container">
  <div class="card mt-5">
    <div class="card-header">
      <h2>Crear registros</h2>
    </div>
    <div class="card-body">

      <form class="" action="" method="post">
        <div class="form-group">
          <label for="email">Marca</label>
          <select class="form-control" name="marca" id="marca">
             <?php foreach ($s as $pers2):?>
            <option value="<?= $pers2->id;?>"><?= $pers2->marca; ?></option>
            <?php endforeach; ?>
            </select>
        </div>
        <div class="form-group">
          <label for="email">Nombre</label><input type="text" name="nombre" id="nombre" class="form-control">
        </div>

        <div class="form-group">
          <label for="email">Descripcion</label><input type="text" name="descripcion" id="descripcion" class="form-control">
        </div>
        <div class="form-group">
          <label for="email">Combustible</label><input type="text" name="combu" id="combu" class="form-control">
        </div>
        <div class="form-group">
          <label for="email">Puertas</label><input type="number" name="puertas" id="puertas" class="form-control">
        </div>
        <div class="form-group">
          <label for="email">Precio</label><input type="number" name="precio" id="precio" class="form-control">
        </div>





        <div class="form-group">
          <button type="submit" name="button" class="btn btn-info">Crear registro</button>
        </div>
      </form>
    </div>
  </div>
</div>

<?php require 'footer.php' ?>
